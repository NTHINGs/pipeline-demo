variable "app_name" {}
variable "tar_path" {}

module "heroku-free-stack" {
  source = "git::https://github.com/NTHINGs/terraform-heroku-free-stack.git?ref=master"

  name = var.app_name

  tar_build_path = "./${var.tar_path}.tar"
}

output "url" {
  value = module.heroku-free-stack.url
}
