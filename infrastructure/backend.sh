#!/bin/sh

#define parameters which are passed in.
BUCKET=$1
KEY=$2

#define the template.
cat  << EOF
terraform {
  backend "gcs" {
    bucket  = "$BUCKET"
    prefix  = "experiments/$KEY"
  }
}
EOF
